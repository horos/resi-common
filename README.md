# Resi Web API Platform

This is still in testing

This platform allows you to implement your API on the server side,
and generate a client side with built in intellisense support.

The package ships with PASETO token management, and support for streaming responses from the server.

This is the common package. It cannot be used on its own.
Please refer to @horos/resi-server or @horos/resi-client for using Resi.
