import { ResiAPIImplementation } from './types-consts';
import { enrich, PlugFunction } from './plugs';

export function createAPIImplementation<T extends Object>(
  name: string,
  apiImplementation: T | (new () => T),
  ...plugs: PlugFunction<ResiAPIImplementation>[]
) {
  let editable = apiImplementation as any;
  // make sure this is a class definition and not an object literal
  if (false === apiImplementation.constructor.toString().startsWith('function Object(')) {
    const Constructor = apiImplementation as new () => T;
    const instance = new Constructor() as any;
    editable = Object.assign(
      {},
      ...Object.getOwnPropertyNames(Constructor.prototype)
        .filter((p) => p !== 'constructor')
        .map((p) => ({ [p]: instance[p] })),
    );
  }

  editable.name = name;
  enrich(editable, ...plugs);
  return editable as T;
}

export * from './comm-std';
