export class StandardResult<T> {
  success: boolean;
  data: T;
  error?: string | object;

  constructor(successVal: boolean, data: T, error?: string | object) {
    this.success = successVal;
    this.data = data;
    this.error = error;
  }

  static failure(error: string | object) {
    return new StandardResult(false, null, error);
  }

  static success<T>(data: T) {
    return new StandardResult(true, data);
  }
}

export type StandardOpResult<T> = Promise<StandardResult<T>>;

export function failure(error: string | object) {
  return StandardResult.failure(error);
}

export function success<T>(data: T) {
  return StandardResult.success(data);
}
