export type ResiHandler = Function & { [key: string]: boolean | any; __params?: string[] };

export type ResiAPIImplementation = {
  [api: string]: APIImplementation;
};

export type APIImplementation = {
  [func: string]: ResiHandler | string;
  name: string;
};

export const API_DIRECTORY = 'apis';
export const MODELS_DIRECTORY = 'models';
export const AUTH_TOKEN_FIELD = 'auth_token';
export const RESI_ROUTE = 'resi';
export const BODY_FIELD_SIGNED = 'resi-signed';
export const BODY_FIELD_ENCRYPTED = 'resi-encrypted';
