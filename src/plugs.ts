import { ResiHandler } from './types-consts';

export const PLUGS = {
  streamResponse: '__stream_response',
  withAuthorization: '__with_autorization',
  httpGet: '__http_get',
  httpPut: '__http_put',
  httpDelete: '__http_delete',
  customRequestBody: '__custom_request_body',
  customHeaders: '__custom_headers',
  roleAuthorization: '__role_authorization',
  cacheServer: '__cache_server',
  cacheClient: '__cache_client',
  signedRequest: '__signed_request',
  encryptedRequest: '__encrypted_request',
};

const HTTP_PLUGS_TO_METHOD_MAP: [string, 'get' | 'put' | 'delete'][] = [
  [PLUGS.httpGet, 'get'],
  [PLUGS.httpPut, 'put'],
  [PLUGS.httpDelete, 'delete'],
];

export const checkPlug = (target: any, plug: string) => target[plug];

export function getHttpMethod(funcImpl: ResiHandler): 'post' | 'get' | 'put' | 'delete' {
  for (const [plug, method] of HTTP_PLUGS_TO_METHOD_MAP) {
    if (checkPlug(funcImpl, plug)) return method;
  }
  return 'post';
}

export type PlugFunction<T> = (target: T) => T;

export function enrich<T>(func: T, ...plugs: PlugFunction<T>[]) {
  plugs.forEach((plug) => plug(func));
  return func;
}
